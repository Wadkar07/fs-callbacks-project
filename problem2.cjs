const fs = require('fs');
function readWriteAndDeleteFileUsingFs(pathVariable) {
    let fileToOperate = pathVariable;
    fs.readFile(fileToOperate, 'utf8', (error, lipsumData) => {
        if (error) {
            console.error(new Error (`Error while reading ${error}`));
            return;
        }
        else {
            lipsumData = lipsumData.toUpperCase();
            let fileNames = [];
            fileToOperate = 'upperCaseLipsum.txt';
            fs.writeFile(fileToOperate, lipsumData, (error) => {
                if (error) {
                    console.error(new Error (`Error while creating file ${fileToOperate} ${error}`));
                    return;
                }
                else {
                    fileToOperate = 'upperCaseLipsum.txt';
                    fileNames.push('upperCaseLipsum.txt');
                    fs.readFile(fileToOperate, 'utf8', (error, lowerCase) => {
                        if (error) {
                            console.error(new Error (`Error while reading file ${fileToOperate} ${error}`));
                            return;
                        }
                        else {
                            lowerCase = lowerCase.toLowerCase()
                                .split('. ')
                                .join('\n');
                            fileToOperate = 'lowerCase.txt';
                            fs.writeFile(fileToOperate, lowerCase, (error) => {
                                if (error) {
                                    console.error(new Error (`Error while creating file ${fileToOperate} ${error}`));
                                    return;
                                }
                                else {
                                    fileToOperate = 'lowerCase.txt';
                                    fileNames.push('lowerCase.txt');
                                    fs.readFile(fileToOperate, 'utf8', (error, lowerCaseConent) => {
                                        if (error) {
                                            console.error(new Error (`Error while reading file ${fileToOperate} ${error}`));
                                            return;
                                        }
                                        else {
                                            lowerCaseConent = lowerCaseConent
                                                .split('\n')
                                                .sort()
                                                .map(line => line.trim())
                                                .join('\n');
                                            fileToOperate = 'sortedLowerCase.txt';
                                            fs.writeFile(fileToOperate, lowerCaseConent, (error) => {
                                                if (error) {
                                                    console.error(new Error (`Error while creating file ${fileToOperate} ${error}`));
                                                    return;
                                                }
                                                else {
                                                    fileNames.push('sortedLowerCase.txt')
                                                    fileToOperate = 'sortedLowerCase.txt';
                                                    fs.readFile(fileToOperate, 'utf8', (error, sortedLowerCaseContent) => {
                                                        if (error) {
                                                            console.error(new Error (`Error while reading file ${fileToOperate} ${error}`));
                                                            return;
                                                        }
                                                        else {
                                                            fileToOperate = 'filenames.txt'
                                                            fs.writeFile(fileToOperate, fileNames.join('\n'), (error) => {
                                                                if (error) {
                                                                    console.error(new Error (`Error while creating file ${fileToOperate} ${error}`));
                                                                }
                                                                else {
                                                                    deleteFiles(0);
                                                                    function deleteFile(fileName, callback) {
                                                                        fs.unlink(fileName, (error) => {
                                                                            if (error) {
                                                                                console.error(new Error (`Error deleting file: ${fileName}`, error));
                                                                                return;
                                                                            }
                                                                            callback();
                                                                        });
                                                                    }
                                                                    function deleteFiles(index) {
                                                                        if (index >= fileNames.length) {
                                                                            return;
                                                                        }
                                                                        else {
                                                                            deleteFile(fileNames[index], () => deleteFiles(index + 1));
                                                                        }
                                                                    };
                                                                }

                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}
module.exports = readWriteAndDeleteFileUsingFs;