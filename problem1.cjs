const fs = require('fs');
const path = require('path');

const createAndDeleteDirectoryAndFiles = (directoryPath = path.join(__dirname, '/newDir'),callback) => {
  fs.mkdir(directoryPath, { recursive: true }, (error) => {
    if (error) {
      callback(new Error (`Error creating directory: ${error}`));
      return;
    }
    else {
      let numFiles = Math.floor(Math.random() * 10);

      const fileNames = Array.from({ length: numFiles }, ((value, index) => {
        return `file${index + 1}.json`
      }));

      const filepaths = fileNames.map((filename) => {
        return path.join(directoryPath, filename)
      });

      const datas = filepaths.map((filepath, index) => {
        return { id: index, name: `Item ${index}` };
      });

      const jsons = datas.map((data) => {
        return JSON.stringify(data)
      });

      const createFile = (filepath, json, createCallback) => {
        fs.writeFile(filepath, json, (error) => {
          if (error) {
            callback(new Error (`Error creating file ${filepath}: ${error}`));
          }else {
            createCallback();
          }
        });
      };

      function createFiles(index) {
        if (index >= filepaths.length) {
          console.log(`${index} Files created!`);
          deleteFiles(0);
          return;
        }
        else {
          createFile(filepaths[index], jsons[index], () => createFiles(index + 1));
        }
      };

      function deleteFile(filePath, deleteCallback) {
        fs.unlink(filePath, (error) => {
          if (error) {
            callback(new Error (`Error deleting file: ${filePath}`, error));
            return;
          }
          else {
            deleteCallback(null);
          }
        });
      }
      function deleteFiles(index) {
        if (index >= filepaths.length) {
          console.log(`${index}  files deleted!`);
          fs.rmdir(directoryPath, (error) => {
            if (error) {
              callback(new Error (`Error deleting directory: ${error}`));
            }
            else {
              console.log('Directory deleted!');
            }
          });
          return;
        }
        else {
          deleteFile(filepaths[index], () => deleteFiles(index + 1));
        }
      };

      createFiles(0);
    }
  });
};

module.exports = createAndDeleteDirectoryAndFiles;